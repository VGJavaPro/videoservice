package com.javapro.util;

import java.io.InputStream;
import java.util.Map;
import java.util.TreeMap;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.mp4.MP4Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javapro.dto.MetaDataDTO;

public class VideoUtil {
	private static final Logger log = LoggerFactory.getLogger(VideoUtil.class);

	/**
	 * 
	 * @author Venu
	 * @param  Inputstream
	 * @return MetaDataDTO transfer Object with duration, content type and Metadata map
	 *
	 */
	public static MetaDataDTO  getMetadataDetails(InputStream inputstream) throws Exception{
		//detecting the file type
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();

		ParseContext pcontext = new ParseContext();

		//HTML Parser
		MP4Parser MP4Parser = new MP4Parser();
		MP4Parser.parse(inputstream, handler, metadata,pcontext);
		log.info("Contents of the document:  :" + handler.toString());		

		MetaDataDTO metaDataDTO = new MetaDataDTO();

		Map<String, String> metaDataMap = new TreeMap<String, String>();

		String[] metadataNames = metadata.names();

		String value = null;
		String key = null;
		for(String name : metadataNames) {
			//Fetch Duration from meta data
			if(name.contains("duration")) {
				metaDataDTO.setVideoDuration(Double.parseDouble(metadata.get(name)));
			}

			if("Content-Type".equals(name)){
				metaDataDTO.setContentType(metadata.get(name));
			}

			key = name;
			value = metadata.get(name);
			metaDataMap.put(key, value);
		}

		//Populate Meta data Map
		metaDataDTO.setMetaDataMap(metaDataMap);

		return metaDataDTO;
	}
}