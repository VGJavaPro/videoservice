package com.javapro.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.javapro.dto.FileDetailDTO;

@Service
public class S3ServiceImpl implements S3Service {

	private Logger log = LoggerFactory.getLogger(S3ServiceImpl.class);
	
	@Autowired
	private AmazonS3 s3client;
 
	@Value("${jpro.s3.bucket}")
	private String bucketName;
	
	@Override
	public void downloadFile(String keyName) {
		try {
			
            System.out.println("Downloading an object");
            S3Object s3object = s3client.getObject(new GetObjectRequest(bucketName, keyName));
            System.out.println("Content-Type: "  + s3object.getObjectMetadata().getContentType());
            S3ObjectInputStream s3ObjectInputStream = s3object.getObjectContent();
            // s3ObjectInputStream is available for further processing
            log.info("===================== Import File - Done! =====================");
            
        } catch (AmazonServiceException ase) {
        	log.info("Caught an AmazonServiceException from GET requests, rejected reasons:");
			log.info("Error Message:    " + ase.getMessage());
			log.info("HTTP Status Code: " + ase.getStatusCode());
			log.info("AWS Error Code:   " + ase.getErrorCode());
			log.info("Error Type:       " + ase.getErrorType());
			log.info("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
        	log.info("Caught an AmazonClientException: ");
            log.info("Error Message: " + ace.getMessage());
        } catch (Exception e) {
        	log.info("Error Message: " + e.getMessage());
		}
	}

	@Override
	public void uploadFile(FileDetailDTO fileDetailDTO) {
		
		try {
			log.info("Data has been created successfully");
			log.info("Uploading a new object to S3 from a file\n");
			ObjectMetadata objectMetaData = new ObjectMetadata();
			//Populate S3 Object metadata
			objectMetaData.setContentLength(fileDetailDTO.getByteLength());
			objectMetaData.setContentType(fileDetailDTO.getContentType());
			s3client.putObject(new PutObjectRequest(
					bucketName, fileDetailDTO.getFileName(), fileDetailDTO.getInputstream(), objectMetaData));
		} catch (AmazonServiceException ase) {
			log.info("Caught an AmazonServiceException, which " +
					"means your request made it " +
					"to Amazon S3, but was rejected with an error response" +
					" for some reason.");
			log.info("Error Message:    " + ase.getMessage());
			log.info("HTTP Status Code: " + ase.getStatusCode());
			log.info("AWS Error Code:   " + ase.getErrorCode());
			log.info("Error Type:       " + ase.getErrorType());
			log.info("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			log.info("Caught an AmazonClientException, which " +
					"means the client encountered " +
					"an internal error while trying to " +
					"communicate with S3, " +
					"such as not being able to access the network.");
			log.info("Error Message: " + ace.getMessage());
		} catch (Exception e){
			log.info("Error While parsing video");
		}
	}

}
