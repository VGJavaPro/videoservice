package com.javapro.service;

import com.javapro.model.User;

public interface UserService {
	public User findUserByUsername(String username);
	public void saveUser(User user);
}
