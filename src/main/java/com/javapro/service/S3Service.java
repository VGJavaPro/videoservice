package com.javapro.service;

import com.javapro.dto.FileDetailDTO;

public interface S3Service {
	public void downloadFile(String keyName);
	public void uploadFile(FileDetailDTO fileDetailDTO);
}
