package com.javapro.dto;

import java.util.Map;

public class MetaDataDTO {
	private double videoDuration;
	private String contentType;
	private Map<String, String> metaDataMap;
	
	public double getVideoDuration() {
		return videoDuration;
	}
	public void setVideoDuration(double videoDuration) {
		this.videoDuration = videoDuration;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Map<String, String> getMetaDataMap() {
		return metaDataMap;
	}
	public void setMetaDataMap(Map<String, String> metaDataMap) {
		this.metaDataMap = metaDataMap;
	}
	
	

}
