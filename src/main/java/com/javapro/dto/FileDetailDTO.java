package com.javapro.dto;

import java.io.InputStream;

public class FileDetailDTO {
	long byteLength;
	private String contentType;
	private String fileName;
	InputStream inputstream;
	
	public long getByteLength() {
		return byteLength;
	}
	public void setByteLength(long byteLength) {
		this.byteLength = byteLength;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public InputStream getInputstream() {
		return inputstream;
	}
	public void setInputstream(InputStream inputstream) {
		this.inputstream = inputstream;
	}
	
}
