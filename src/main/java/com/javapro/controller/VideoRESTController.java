package com.javapro.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.javapro.dto.FileDetailDTO;
import com.javapro.dto.MetaDataDTO;
import com.javapro.service.S3Service;
import com.javapro.util.VideoUtil;

@RestController
public class VideoRESTController {

	@Autowired
	S3Service s3Service;

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 *  
	 * This method uploads MP4 files to Amazon S3 bucket.
	 * 
	 * @author Venu
	 *
	 * @param file  MP4 file 
	 * 
	 * @return Returns Video meta data map or error message
	 * 
	 */
	@RequestMapping(value="/admin/upload", method= RequestMethod.POST, consumes=MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> uploadVideoToS3(@RequestParam("file") MultipartFile file) {
		String errorMessage = null;
		double videoDuration = 0;
		String contentType = null;
		MetaDataDTO metaDataDTO = null;
		int byteLength = 0;
		String fileName = null;
		FileDetailDTO fileDetailDTO = null;

		if (file.isEmpty()) {
			return new ResponseEntity("Please select a file!", HttpStatus.OK);
		} else {

			try {
				fileName = file.getOriginalFilename();

				byte[] bytes = file.getBytes();

				byteLength = bytes.length;

				InputStream inputstream = new ByteArrayInputStream(bytes);

				metaDataDTO = VideoUtil.getMetadataDetails(inputstream);
				videoDuration = metaDataDTO.getVideoDuration();
				contentType =  metaDataDTO.getContentType();

				fileDetailDTO = new FileDetailDTO();

				fileDetailDTO.setByteLength(byteLength);
				fileDetailDTO.setContentType(contentType);
				fileDetailDTO.setFileName(fileName);
				fileDetailDTO.setInputstream(inputstream);

				if(contentType.contains("mp4")){
					inputstream.reset();
					s3Service.uploadFile(fileDetailDTO);
					return new ResponseEntity<Map<String, String>>(metaDataDTO.getMetaDataMap(), HttpStatus.OK);
				} else {
					if(!contentType.contains("mp4")){
						errorMessage = "Invalid Video type. Please upload MP4 files.";
						log.info(errorMessage);
						return new ResponseEntity<String>(errorMessage, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
					} else {
						errorMessage = "Invalid Request. Please try again.";
						log.info(errorMessage);
						return new ResponseEntity<String>(errorMessage, HttpStatus.BAD_REQUEST);
					}
				}
			} catch (IOException e) {
				log.info("Exception occured during file access");
				errorMessage = "Exception occured during file access";
				return new ResponseEntity<String>(errorMessage, HttpStatus.BAD_REQUEST);

			}
			catch (Exception e1) {
				errorMessage = "Exception occured during video upload. Please try again.";
				log.info("Exception during Video file conversion");
				return new ResponseEntity<String>(errorMessage, HttpStatus.BAD_REQUEST);
			}

		}
	}
}
