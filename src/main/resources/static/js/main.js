$(document).ready(function () {

    $("#btnSubmit").click(function (event) {
    	
    	if (!$("#video-file").val()) {
    		   $('#uploadError').text("File is required");
    		   return false;
    	} 

        //stop submit the form, post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });
    
    $('#videoDetails').hide(); //Reset details title on load
 });

function fire_ajax_submit() {

    // Get form
    var form = $('#fileUploadForm')[0];

    var data = new FormData(form);

    $("#btnSubmit").prop("disabled", true); //Disable button to avoid duplicate upload
  
  	$('#result').empty(); //Reset Video detail table
 
  	$('#errorDetail').text(null); //Reset Error field
  	
  	 $('#videoDetails').hide(); //Reset details title
  	
    $.ajax({
    	type: "POST",
    	enctype: 'multipart/form-data',
    	url: "/admin/upload",
    	data: data,
     	processData: false, //Prevents jQuery from transforming the data into a query string
    	contentType: false,
    	cache: false,
    	timeout: 60000000,
    	success: function (data) {
    		var trHTML = '';
    		$('#videoDetails').show(); //Show Details section
    		var header = '<tr><th>Key</th><th>Value</th></tr>';
    		$('#result').append(header);
    		if($.isEmptyObject(data)){
    			trHTML += '<tr><td>' + 'Video upload was not successful! Please try again'+ '</td></tr>';
    		} else {
    			$.each(data, function(key, value) {
        			trHTML += '<tr><td>' + key + '</td><td>' + value + '</td></tr>';
        		});
    			$('#result').append(trHTML);
    		}
    		console.log("SUCCESS : ", data);
    		$("#btnSubmit").prop("disabled", false);
    	},
    	error: function (e) {
    		$('#errorDetail').text(e.responseText);
    		console.log("ERROR : ", e);
    		$("#btnSubmit").prop("disabled", false);
    	}
    });

}