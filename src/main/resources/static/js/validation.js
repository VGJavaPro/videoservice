$("#video-file").change(function () {
    var fileExtension = ['mp4'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        $('#uploadError').text("Please upload only mp4 files"); 
        $("#btnSubmit").prop("disabled", true); //Disable button for invalid input
    } else if(this.files[0].size > 41943040) {
    	$('#uploadError').text("Files must be less than 40MB");
    	$("#btnSubmit").prop("disabled", true); //Disable button for invalid input
    } else {
    	$('#uploadError').text(null); //Reset error field
    	$("#btnSubmit").prop("disabled", false); //Enable button for valid input
    }
});





 
