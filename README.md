# Video Spring REST Web Service

1. mvn clean
2. mvn clean install
3. Go to the target folder
4. java -jar videoService-0.0.1-SNAPSHOT.jar
5. The deployment created database structure. Execute insert_roles.sql for inserting users.
   
This script create one ADMIN and one USER role. ADMIN has access for video uploading. USER has no authorization for this resource

Service URL
- http://localhost:8080/

By default registration adds user ADMIN. For testing please create one more user and update role_id to 2 in user-role table.

Service description.

Register admin can upload only MP4 files to Amazon S3. The service will display MP4 video parameters after the upload.

URL for checking file uploads.
- http://s3-external-1.amazonaws.com/s3videouser/

